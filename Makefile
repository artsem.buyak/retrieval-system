.EXPORT_ALL_VARIABLES:
COMPOSE_FILE ?= docker/docker-compose-local.yml
COMPOSE_PROJECT_NAME ?= retrieval_system

DOTENV_BASE_FILE ?= .env-base
DOTENV_CUSTOM_FILE ?= .env-custom

POETRY_EXPORT_WITHOUT_INDEXES ?= true
POETRY_EXPORT_OUTPUT = requirements.txt
POETRY_VERSION = 1.3.2
POETRY ?= $(HOME)/.local/bin/poetry

PYTHON_INSTALL_PACKAGES_USING ?= poetry
DB_MIGRATIONS_CONFIG = src/db/migrations/alembic.ini

mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
mkfile_dir := $(dir $(mkfile_path))

export PYTHONPATH=:$(CURDIR)/src:$(mkfile_dir):$(mkfile_dir)/src

-include $(DOTENV_BASE_FILE)
-include $(DOTENV_CUSTOM_FILE)

.PHONY: install-poetry
install-poetry:
	curl -sSL https://install.python-poetry.org | python3 -
	$(POETRY) config virtualenvs.create true
	$(POETRY) run pip install --upgrade pip

.PHONY: install-packages
install-packages:
	$(POETRY) install -vv $(opts)

.PHONY: activate
activate:
	$(POETRY) shell

.PHONY: remove
remove:
	$(POETRY) env remove $(ENV_NAME)

.PHONY: install
install: install-poetry install-packages

.PHONY: update
update:
	$(POETRY) update -v

.PHONY: migrate
migrate:
	$(POETRY) run alembic --config $(DB_MIGRATIONS_CONFIG) upgrade head

# command example: `make downgrade MIGRATION='fb1f4b788f03'`
.PHONY: downgrade
downgrade:
	$(POETRY) run alembic --config $(DB_MIGRATIONS_CONFIG) downgrade ${MIGRATION}

.PHONY: migrations
migrations:
	$(POETRY) run alembic --config $(DB_MIGRATIONS_CONFIG) revision --autogenerate --message auto

.PHONY: service
service:
	$(POETRY) run python src/core/main.py

.PHONY: docker-lint
docker-lint:
	docker run --rm -i replicated/dockerfilelint:ad65813 < ./docker/Dockerfile

.PHONY: docker-build
docker-build:
	@docker build \
		--tag=retrieval_system \
		--file=docker/Dockerfile \
		--build-arg BUILD_RELEASE=dev \
		.


.PHONY: docker-build-m1
docker-build-m1:
	@docker buildx build --platform linux/amd64 \
		--tag=retrieval_system \
		--file=docker/Dockerfile \
		--build-arg BUILD_RELEASE=dev \
		.


.PHONY: docker-tag
docker-tag:
	@docker tag retrieval_system:latest $(DOCKER_HUB):$(TAG_VERSION)


.PHONY: docker-push
docker-push:
	# Before run make docker login, example:
	# aws ecr get-login-password | docker login --username AWS --password-stdin YOUR_DOCKER_HUB
	@docker push $(DOCKER_HUB):$(TAG_VERSION)

.PHONY: docker-up
docker-up:
	docker-compose up --remove-orphans -d
	docker-compose ps

.PHONY: docker-down
docker-down:
	docker-compose down

.PHONY: docker-logs
docker-logs:
	docker-compose logs --follow

.PHONY: lint-bandit
lint-bandit:
	$(POETRY) run bandit --ini .bandit --recursive

.PHONY: lint-black
lint-black:
	$(POETRY) run black --check --diff .

.PHONY: lint-flake8
lint-flake8:
	$(POETRY) run flake8

.PHONY: lint-isort
lint-isort:
	$(POETRY) run isort --check-only --diff .

.PHONY: lint-mypy
lint-mypy:
	$(POETRY) run mypy

.PHONY: lint
lint: lint-bandit lint-black lint-flake8 lint-isort  # lint-mypy

.PHONY: fmt
fmt:
	$(POETRY) run isort .
	$(POETRY) run black .

.PHONY: test
test:
	$(POETRY) run pytest $(opts) $(call tests,.) --disable-pytest-warnings
