import logging
from http import HTTPStatus
from random import SystemRandom
from typing import Any

from aiopinecone.schemas.generated import QueryResponse
from fastapi import Body, Depends
from fastapi.responses import StreamingResponse
from fastapi_utils.cbv import cbv
from fastapi_utils.inferring_router import InferringRouter
from sqlalchemy.ext.asyncio import AsyncSession

from api import deps
from clients.pinecone import pinecone_client
from core.config import settings
from db.managers.pages import paragraph_fact_manager
from ml_tools.onnx_text_embedding import text_embedder
from ml_tools.response_generation import response_generator

router = InferringRouter()

PREFIX = "/facts-retrieval"

NOT_FOUND_RESPONSES: list[str] = [
    "I'm sorry, but we were unable to find any information on this topic in our database. "
    "Would you like us to perform another search or do you have any other questions?",
    "Unfortunately, we don't have enough data to provide a confident response to your query."
    " Would you like to try rephrasing your question or provide us with more details?",
    "It seems that our database doesn't contain enough information to give you a reliable answer to your question."
    " Would you like us to refer you to other sources of information or experts on this topic?",
    "We apologize for the inconvenience, but we were unable to find any relevant data in our database. "
    "Would you like us to perform a manual search or consult with our team of researchers to find an answer?",
    "Our apologies, it appears that we don't have enough information to provide a definitive response to "
    "your query. Would you like us to keep searching or do you have any other questions that we can help with?",
]

logger = logging.getLogger()


def streaming_response_gen(text: str) -> str:
    for item in text.split(" "):
        yield f"{item} "


@cbv(router)
class OpenAI:
    session: AsyncSession = Depends(deps.get_session)
    system_random = SystemRandom()

    @router.post(
        PREFIX,
        status_code=HTTPStatus.OK,
        name="fact-retrieval-create",
    )
    async def post(
        self,
        *,
        question: str = Body(..., embed=True),
    ) -> Any:
        """
        Retrieve text completion facts + OpenAI completion
        """
        logger.info(f"Retrieve call with question: {question}")

        embedding = await text_embedder.generate(
            sentences=[
                question,
            ]
        )

        nearest_vector: QueryResponse = await pinecone_client.search_by_vector(
            vector=embedding[-1],
        )

        if nearest_vector.matches[0].score < settings.SCORE_THRESHOLD:
            logger.debug(
                f"Retrieve with too small threshold: {question}, "
                f"{nearest_vector.matches[0].id}, "
                f"{nearest_vector.matches[0].score}"
            )
            return StreamingResponse(
                streaming_response_gen(
                    text=NOT_FOUND_RESPONSES[self.system_random.randrange(len(NOT_FOUND_RESPONSES))],
                ),
                media_type="text/event-stream",
            )

        fact = await paragraph_fact_manager.get(
            self.session,
            obj_id=int(nearest_vector.matches[0].id),
            use_paragraph_join=True,
        )

        return StreamingResponse(
            response_generator.generate(
                question=question,
                paragraph=fact.paragraph.text,
                fact=fact.text,
            ),
            media_type="text/event-stream",
        )
