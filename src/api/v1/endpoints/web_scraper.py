from http import HTTPStatus
from typing import Any
from urllib.parse import urlparse

from fastapi import BackgroundTasks, Depends, HTTPException
from fastapi_utils.cbv import cbv
from fastapi_utils.inferring_router import InferringRouter
from sqlalchemy.ext.asyncio import AsyncSession

from api import deps
from api.v1.tasks.page_info_processing import parse_info_by_url
from core.config import settings
from schemas.url_parser import UrlParserRequest

router = InferringRouter()

PREFIX = "/url-parser"


@cbv(router)
class UrlParser:
    session: AsyncSession = Depends(deps.get_session)
    available_domains = settings.AVAILABLE_DOMAINS

    @router.post(
        PREFIX,
        status_code=HTTPStatus.ACCEPTED,
        name="url-parse-create",
    )
    async def post(
        self,
        *,
        data: UrlParserRequest,
        background_tasks: BackgroundTasks,
    ) -> Any:
        """
        Retrieve page by url and fill vectors db in background
        """

        domain = urlparse(data.source_url).netloc

        if domain not in self.available_domains:
            raise HTTPException(
                status_code=HTTPStatus.BAD_REQUEST,
                detail="Sorry, currently we support urls only from wikipedia domain.",
            )

        background_tasks.add_task(parse_info_by_url, url=data.source_url, session=self.session)
