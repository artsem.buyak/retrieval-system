import asyncio
import hashlib
import logging

import aiohttp
from aiopinecone.schemas.generated import Vector
from bs4 import BeautifulSoup
from pydantic import HttpUrl
from sqlalchemy.ext.asyncio import AsyncSession

from api.deps import DbSession
from clients.pinecone import pinecone_client
from core.config import settings
from db.managers.pages import (
    page_manager,
    page_task_manager,
    paragraph_fact_manager,
    paragraph_manager,
)
from db.models.pages import Page, PageTask, Paragraph, ParagraphFact, StatusEnum
from ml_tools.facts_extraction import facts_extractor
from ml_tools.onnx_text_embedding import text_embedder
from schemas import (
    PageCreate,
    PageTaskCreate,
    PageTaskUpdate,
    PageUpdate,
    ParagraphCreate,
)

WIKIPEDIA_TEXT_BODY_ID = "mw-content-text"
PARAGRAPH_MIN_WORDS = 10

logger = logging.getLogger()


async def download_page_by_url(url: str) -> str:
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as response:
            return await response.text()


async def processing_paragraph_info(
    text: str,
    paragraph_id: int,
):
    logger.info(f"`processing_paragraph_info` call: {paragraph_id}, {text}")
    facts = await facts_extractor.extract(text=text)
    embeddings = await text_embedder.generate(sentences=facts)
    vectors = []

    async with DbSession() as session:
        fact_objs = await paragraph_fact_manager.batch_create(
            session,
            paragraph_id=paragraph_id,
            facts_embeddings=zip(facts, embeddings),
        )

        for fact_id, embedding in zip([item.id for item in fact_objs], embeddings):
            logger.debug(f"save fact info: {paragraph_id}, {fact_id}, {embedding}")

            vectors.append(
                Vector(
                    id=fact_id,
                    values=embedding,
                )
            )

        await pinecone_client.batch_insert(
            vectors=vectors,
            namespace=settings.PINECONE_DEFAULT_NAMESPACE,
        )


async def cleanup_previous_processing_results(
    page_id: int,
    session: AsyncSession,
):
    paragraph_objs: list[Paragraph] = await paragraph_manager.get_multi_by_page_id(
        session,
        page_id=page_id,
    )

    paragraph_fact_objs: list[ParagraphFact] = await paragraph_fact_manager.get_multi_by_paragraphs_ids(
        session,
        paragraphs_ids=[obj.id for obj in paragraph_objs],
    )

    await pinecone_client.delete_vectors(vector_ids=[obj.id for obj in paragraph_fact_objs])

    await paragraph_manager.remove_by_page_id(
        session,
        page_id=page_id,
    )


async def is_page_processed_before(
    url: str,
    checksum: str,
    session: AsyncSession,
) -> tuple[Page, PageTask, bool]:
    page_task_obj = None
    page_obj = await page_manager.get_by_url(
        session,
        url=str(url),
    )

    if page_obj:
        page_task_obj_list = await page_task_manager.get_multi_by_page_id(
            session,
            page_id=page_obj.id,
        )

        page_task_obj = page_task_obj_list[-1]

        if page_obj.checksum == checksum and page_task_obj.status != StatusEnum.FAILED.value:
            return page_obj, page_task_obj, True
        else:
            await page_manager.update(
                session,
                db_obj=page_obj,
                obj_in=PageUpdate(
                    checksum=checksum,
                ),
            )
            page_task_obj = await page_task_manager.create(
                session,
                obj_in=PageTaskCreate(
                    page_id=page_obj.id,
                ),
            )

            await cleanup_previous_processing_results(
                page_id=page_obj.id,
                session=session,
            )

    if not page_obj:
        page_obj = await page_manager.create(
            session,
            obj_in=PageCreate(
                url=str(url),
                checksum=checksum,
            ),
        )

        page_task_obj = await page_task_manager.create(
            session,
            obj_in=PageTaskCreate(
                page_id=page_obj.id,
            ),
        )

    return page_obj, page_task_obj, False


async def parse_info_by_url(url: HttpUrl, session: AsyncSession):
    logger.info(f"`parse_info_by_url` call: {str(url)}")
    # get URL
    page_content = await download_page_by_url(url)

    checksum = hashlib.sha256(page_content.encode("utf-8")).hexdigest()

    page_obj, page_task_obj, is_processed = await is_page_processed_before(
        url=str(url),
        checksum=checksum,
        session=session,
    )

    if is_processed:
        return

    # scrape webpage
    soup = BeautifulSoup(page_content, "html.parser")
    try:
        clean_paragraphs = [
            item.text.strip()
            for item in soup.find(id=WIKIPEDIA_TEXT_BODY_ID).findAll("p")
            if item and item.text.strip() and len(item.text.strip().split(" ")) > PARAGRAPH_MIN_WORDS
        ]
    except Exception as exp:
        logger.error(f"Error during parse url: {str(exp)}")

        await page_task_manager.update(
            session,
            db_obj=page_task_obj,
            obj_in=PageTaskUpdate(
                status=StatusEnum.FINISHED.value,
            ),
        )
        return

    try:
        tasks = []
        for index, elem in enumerate(clean_paragraphs):
            paragraph_obj = await paragraph_manager.create(
                session,
                obj_in=ParagraphCreate(
                    text=elem,
                    page_id=page_obj.id,
                    order=index,
                ),
            )

            tasks.append(
                processing_paragraph_info(
                    text=elem,
                    paragraph_id=paragraph_obj.id,
                )
            )

        await asyncio.gather(*tasks)
    except Exception as exp:
        await page_task_manager.update(
            session,
            db_obj=page_task_obj,
            obj_in=PageTaskUpdate(
                status=StatusEnum.FAILED.value,
                error_details=str(exp),
            ),
        )
    else:
        await page_task_manager.update(
            session,
            db_obj=page_task_obj,
            obj_in=PageTaskUpdate(
                status=StatusEnum.FINISHED.value,
            ),
        )
