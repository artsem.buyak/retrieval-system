from fastapi import APIRouter

from api.v1.endpoints import facts_retrieval, web_scraper

api_router = APIRouter()

api_router.include_router(web_scraper.router, tags=["web-scraper"])
api_router.include_router(facts_retrieval.router, tags=["facts-retrieval"])
