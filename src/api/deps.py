from sqlalchemy.ext.asyncio import AsyncSession

from db.models.session import DbSession


# Dependency
async def get_session() -> AsyncSession:
    async with DbSession() as session:
        yield session
