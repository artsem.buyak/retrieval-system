import os
import tempfile

from async_lru import alru_cache
from onnxruntime import InferenceSession
from transformers import AutoTokenizer

from clients.s3 import s3_client

_BATCH_SIZE = 8
_MAX_LENGTH = 512


class TextEmbedder:
    """
    Class for generating embeddings from sentences
    """

    def __init__(self, version: int = 1):
        self.version = version
        self._padding = True
        self._truncation = True
        self._max_length = _MAX_LENGTH
        self._s3_client = s3_client

    @alru_cache(maxsize=1)
    async def _session(self):
        """
        Download model from s3
        """
        raw_model = await s3_client.download_file(f"onnx/{self.version}/model.onnx")

        return InferenceSession(
            raw_model,
            providers=[
                "CUDAExecutionProvider",
                "CPUExecutionProvider",
            ],
        )

    @alru_cache(maxsize=1)
    async def _tokenizer(self):
        """
        Download tokenizer from s3
        """

        bucket_objs = await s3_client.get_list_objects(
            prefix_path=f"onnx/{self.version}/tokenizer",
        )
        with tempfile.TemporaryDirectory() as tempdir:
            for file_path in bucket_objs:
                raw_file = await s3_client.download_file(
                    file_path=file_path,
                )
                with open(os.path.join(tempdir, os.path.basename(file_path)), "wb+") as file:
                    file.write(raw_file)

            return AutoTokenizer.from_pretrained(tempdir, local_files_only=True)

    @staticmethod
    def get_batch(iterable, n=1):
        """
        Split iterable elem by n size
        """

        for ndx in range(0, len(iterable), n):
            yield iterable[ndx : min(ndx + n, len(iterable))]

    async def generate(self, sentences: list[str]) -> list[list[float]]:
        """
        Generate embeddings from list of sentences

        :param sentences: list of sentences ["test_1", "test_2", ...]

        :return embedding for each input sentence

        """
        if not isinstance(sentences, list):
            raise ValueError("Calling embeddings service with incorrect arguments.")

        embeddings = []
        session = await self._session()
        tokenizer = await self._tokenizer()

        for batch in self.get_batch(sentences, _BATCH_SIZE):
            inputs = tokenizer(
                batch,
                padding=self._padding,
                truncation=self._truncation,
                max_length=self._max_length,
                return_tensors="np",
            )
            outputs = session.run(
                None,
                {
                    "input_ids": inputs.input_ids,
                    "attention_mask": inputs.attention_mask,
                },
            )[0]
            embeddings.extend(outputs.tolist())

        return embeddings


text_embedder = TextEmbedder()
