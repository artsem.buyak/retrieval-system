import regex as re

from clients.openai import openai_client

_TEMPERATURE = 0.1
PROMPT = """Please extract the key facts from the text provided. Each fact should be informative and concise.
Text:

{text}

You need to take into account:
- Replace all pronouns by names for each fact.
- Format the result as a list of strings, with each fact separated by a hyphen (-).

Result:"""


class FactsExtractor:
    """
    Class for extract list of facts by input sentence
    """

    def __init__(self):
        self._openai_client = openai_client

    def build_prompt(self, text: str) -> list[dict[str, str]]:
        """
        Serialize necessary llm model input format
        """

        return [
            {
                "role": "user",
                "content": PROMPT.format(text=text),
            },
        ]

    @staticmethod
    def _parse_response(response: str) -> list[str]:
        """
        Split response by `-` separator

        :param response: text response from llm model
        :return: list of facts (str)
        """
        return re.findall(r"-\s*(.*)", response, flags=re.MULTILINE)

    async def extract(self, text: str) -> list[str]:
        """
        Make llm model call for extracting facts

        :param text: text for extracting facts
        :return: list of facts (str)
        """

        messages = self.build_prompt(text)
        response = await self._openai_client.get_completion(messages, temperature=_TEMPERATURE)
        return self._parse_response(response)


facts_extractor = FactsExtractor()
