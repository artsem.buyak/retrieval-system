from typing import AsyncGenerator

from clients.openai import openai_client

PROMPT = """ There is a text with some information:

{paragraph}

There is the most relevant fact to the question:
{fact}

Answer on question:
{question}

Answer:"""

_TEMPERATURE = 0.7


class ResponseGenerator:
    def __init__(self):
        self._openai_client = openai_client

    @staticmethod
    def build_prompt(question: str, paragraph: str, fact: str) -> list[dict[str, str]]:
        """
        Serialize necessary llm model input format
        """

        return [
            {
                "role": "user",
                "content": PROMPT.format(question=question, paragraph=paragraph, fact=fact),
            },
        ]

    def generate(self, question: str, paragraph: str, fact: str) -> AsyncGenerator:
        """
        Make llm model call and stream response

        :param question: input question for answering
        :param paragraph: full paragraph text
        :param fact: the most relevant fact to question
        :return: list of facts (str)
        """

        messages = self.build_prompt(question, paragraph, fact)
        return self._openai_client.get_completion_stream(messages, temperature=_TEMPERATURE)


response_generator = ResponseGenerator()
