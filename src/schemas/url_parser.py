from pydantic import BaseModel, HttpUrl


class UrlParserRequest(BaseModel):
    source_url: HttpUrl
