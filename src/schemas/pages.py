from pydantic import BaseModel

from db.models.pages import StatusEnum


class PageBase(BaseModel):
    checksum: str | None = None


# Shared properties
class PageCreate(PageBase):
    url: str


class PageUpdate(PageBase):
    pass


class PageTaskBase(BaseModel):
    status: str = StatusEnum.PROCESSING.value


# Shared properties
class PageTaskCreate(PageTaskBase):
    page_id: int


class PageTaskUpdate(PageTaskBase):
    error_details: str | None = None


class ParagraphCreate(BaseModel):
    text: str
    page_id: int
    order: int | None = None


class ParagraphFactCreate(BaseModel):
    text: str
    embedding: list[float]
    paragraph_id: int
