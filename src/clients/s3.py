import io
import logging

from aioboto3 import Session

from clients.exceptions import MaxAttemptError
from core.config import settings

MAX_ATTEMPT = 5

logger = logging.getLogger()


class S3Client:
    def __init__(self):
        self.session = Session()

    async def get_list_objects(self, prefix_path: str) -> list[str]:
        async with self.session.client(
            "s3",
            endpoint_url=settings.AWS_S3_ENDPOINT_URL,
            aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
            aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
        ) as s3:
            for _ in range(MAX_ATTEMPT):
                try:
                    objs = await s3.list_objects(
                        Bucket=settings.AWS_S3_BUCKET_NAME,
                        Prefix=prefix_path,
                    )

                    return [item["Key"] for item in objs["Contents"]]
                except Exception as exp:  # nosec
                    logger.error(f"Error during s3 call: {str(exp)}")
                continue

        raise MaxAttemptError(
            message="s3 responses are not success.",
        )

    async def download_file(self, file_path: str) -> bytes:
        async with self.session.client(
            "s3",
            endpoint_url=settings.AWS_S3_ENDPOINT_URL,
            aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
            aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
        ) as s3:
            for _ in range(MAX_ATTEMPT):
                try:
                    s3_ob = await s3.get_object(
                        Bucket=settings.AWS_S3_BUCKET_NAME,
                        Key=file_path,
                    )

                    async with s3_ob["Body"] as stream:
                        return await stream.read()
                except Exception as exp:  # nosec
                    logger.error(f"Error during s3 call: {str(exp)}")
                continue

        raise MaxAttemptError(
            message="s3 responses are not success.",
        )

    async def copy_file(self, source_path: str, target_path: str) -> None:
        async with self.session.client(
            "s3",
            endpoint_url=settings.AWS_S3_ENDPOINT_URL,
            aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
            aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
        ) as s3:
            for _ in range(MAX_ATTEMPT):
                try:
                    await s3.copy(
                        CopySource={
                            "Bucket": settings.AWS_S3_BUCKET_NAME,
                            "Key": source_path,
                        },
                        Bucket=settings.AWS_S3_BUCKET_NAME,
                        Key=target_path,
                    )
                except Exception as exp:  # nosec
                    logger.error(f"Error during s3 call: {str(exp)}")
                continue

        raise MaxAttemptError(
            message="s3 responses are not success.",
        )

    async def remove_file(self, file_path: str) -> None:
        async with self.session.client(
            "s3",
            endpoint_url=settings.AWS_S3_ENDPOINT_URL,
            aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
            aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
        ) as s3:
            for _ in range(MAX_ATTEMPT):
                try:
                    await s3.delete_object(
                        Bucket=settings.AWS_S3_BUCKET_NAME,
                        Key=file_path,
                    )
                except Exception as exp:  # nosec
                    logger.error(f"Error during s3 call: {str(exp)}")
                continue

        raise MaxAttemptError(
            message="s3 responses are not success.",
        )

    async def upload_file(self, file_path: str, file_obj: io.BytesIO) -> None:
        async with self.session.client(
            "s3",
            endpoint_url=settings.AWS_S3_ENDPOINT_URL,
            aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
            aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
        ) as s3:
            for _ in range(MAX_ATTEMPT):
                try:
                    await s3.upload_fileobj(
                        file_obj,
                        settings.AWS_S3_BUCKET_NAME,
                        file_path,
                    )
                except Exception as exp:  # nosec
                    logger.error(f"Error during s3 call: {str(exp)}")
                continue

        raise MaxAttemptError(
            message="s3 responses are not success.",
        )


s3_client = S3Client()
