class MaxAttemptError(Exception):
    """
    Connection error during reading response.
    """

    def __init__(
        self,
        message: str = "",
    ) -> None:
        self.message = message

    def __str__(self) -> str:
        return "message={!r}".format(
            self.message,
        )
