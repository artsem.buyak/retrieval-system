import itertools
import logging

import pinecone
from aiohttp.client_exceptions import ClientResponseError
from aiopinecone.client import PineconeVectorClient
from aiopinecone.schemas.generated import (
    DeleteRequest,
    IndexMeta,
    QueryRequest,
    QueryResponse,
    UpsertRequest,
    UpsertResponse,
    Vector,
)

from clients.exceptions import MaxAttemptError
from core.config import settings

MAX_ATTEMPT = 5

logger = logging.getLogger()

pinecone.init(
    api_key=settings.PINECONE_API_KEY,
    environment=settings.PINECONE_ENV,
)


class PineconeClient:
    @staticmethod
    def chunks(iterable, batch_size=100):
        """
        A helper function to break an iterable into chunks of size batch_size.
        """
        it = iter(iterable)
        chunk = tuple(itertools.islice(it, batch_size))
        while chunk:
            yield chunk
            chunk = tuple(itertools.islice(it, batch_size))

    def create_index(
        self,
        index_name: str | None = None,
    ) -> None:
        pinecone.create_index(
            name=index_name or settings.PINECONE_INDEX_NAME,
            dimension=settings.PINECONE_INDEX_DIMENSION,
        )

    async def describe_index(
        self,
        index_name: str | None = None,
    ) -> IndexMeta | None:
        async with PineconeVectorClient(
            api_key=settings.PINECONE_API_KEY,
            region=settings.PINECONE_ENV,
            index=index_name or settings.PINECONE_INDEX_NAME,
            project_id=settings.PINECONE_PROJECT_ID,
        ) as client:
            for _ in range(MAX_ATTEMPT):
                try:
                    return await client.describe_index()
                except ClientResponseError:
                    return
                except Exception as exp:  # nosec
                    logger.error(f"Error during pinecone call: {str(exp)}")
                    continue

            raise MaxAttemptError(
                message="Pinecone responses are not success.",
            )

    async def search_by_vector(
        self,
        vector: list[float],
        namespace: str | None = None,
        index_name: str | None = None,
    ) -> QueryResponse | None:
        async with PineconeVectorClient(
            api_key=settings.PINECONE_API_KEY,
            region=settings.PINECONE_ENV,
            index=index_name or settings.PINECONE_INDEX_NAME,
            project_id=settings.PINECONE_PROJECT_ID,
        ) as client:
            for _ in range(MAX_ATTEMPT):
                try:
                    data = QueryRequest(
                        vector=vector,
                        namespace=namespace or settings.PINECONE_DEFAULT_NAMESPACE,
                        topK=1,
                    )

                    return await client.query(request=data)
                except Exception as exp:  # nosec
                    logger.error(f"Error during pinecone call: {str(exp)}")
                    continue

            raise MaxAttemptError(
                message="Pinecone responses are not success.",
            )

    async def delete_vectors(
        self,
        vector_ids: list[int],
        namespace: str | None = None,
        index_name: str | None = None,
    ) -> QueryResponse | None:
        async with PineconeVectorClient(
            api_key=settings.PINECONE_API_KEY,
            region=settings.PINECONE_ENV,
            index=index_name or settings.PINECONE_INDEX_NAME,
            project_id=settings.PINECONE_PROJECT_ID,
        ) as client:
            for _ in range(MAX_ATTEMPT):
                try:
                    data = DeleteRequest(
                        ids=vector_ids,
                        namespace=namespace or settings.PINECONE_DEFAULT_NAMESPACE,
                    )

                    return await client.delete(request=data)
                except Exception as exp:  # nosec
                    logger.error(f"Error during pinecone call: {str(exp)}")
                continue

        raise MaxAttemptError(
            message="Pinecone responses are not success.",
        )

    async def batch_insert(
        self,
        vectors: list[Vector] | None = None,
        namespace: str | None = None,
        index_name: str | None = None,
    ) -> list[UpsertResponse]:
        async with PineconeVectorClient(
            api_key=settings.PINECONE_API_KEY,
            region=settings.PINECONE_ENV,
            index=index_name or settings.PINECONE_INDEX_NAME,
            project_id=settings.PINECONE_PROJECT_ID,
        ) as client:
            for _ in range(MAX_ATTEMPT):
                try:
                    result = []
                    for chunk in self.chunks(iterable=vectors):
                        result.append(
                            await client.upsert(
                                UpsertRequest(
                                    vectors=chunk,
                                    namespace=namespace or settings.PINECONE_DEFAULT_NAMESPACE,
                                ),
                            )
                        )

                    return result
                except Exception as exp:  # nosec
                    logger.error(f"Error during pinecone call: {str(exp)}")
                continue

        raise MaxAttemptError(
            message="Pinecone responses are not success.",
        )


pinecone_client = PineconeClient()
