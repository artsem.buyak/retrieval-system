import logging

import openai

from clients.exceptions import MaxAttemptError
from core.config import settings

openai.api_key = settings.OPENAI_KEY
MODEL_NAME = "gpt-3.5-turbo-0301"

MAX_ATTEMPT = 5

logger = logging.getLogger()


class OpenaiClient:
    async def get_completion(
        self,
        messages: list[dict[str, str]],
        temperature: float = 0.7,
    ) -> str:
        for _ in range(MAX_ATTEMPT):
            try:
                completion = await openai.ChatCompletion.acreate(
                    temperature=temperature,
                    top_p=1,
                    max_tokens=512,
                    model=MODEL_NAME,
                    messages=messages,
                )

                return completion["choices"][0]["message"]["content"]
            except Exception as exp:  # nosec
                logger.error(f"Error during openai call: {str(exp)}")
                continue

        raise MaxAttemptError(
            message="Openai responses are not success.",
        )

    async def get_completion_stream(
        self,
        messages: list[dict[str, str]],
        temperature: float = 0.7,
    ) -> str:
        for _ in range(MAX_ATTEMPT):
            try:
                async for chunk in await openai.ChatCompletion.acreate(
                    temperature=temperature,
                    top_p=1,
                    max_tokens=512,
                    model=MODEL_NAME,
                    messages=messages,
                    stream=True,
                ):
                    content = chunk["choices"][0].get("delta", {}).get("content")
                    if content is not None:
                        yield content
                return
            except Exception as exp:  # nosec
                logger.error(f"Error during openai call: {str(exp)}")
                continue

        raise MaxAttemptError(
            message="Openai responses are not success.",
        )


openai_client = OpenaiClient()
