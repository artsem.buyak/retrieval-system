from typing import Any

from sqlalchemy import Column, DateTime
from sqlalchemy.ext.declarative import as_declarative, declared_attr
from sqlalchemy.sql import func


@as_declarative()
class Base:
    id: Any
    __name__: str

    @declared_attr
    def __tablename__(cls) -> str:
        return cls.__name__.lower()


class CreateUpdateMixin:
    # datetimes
    created_at = Column(
        DateTime,
        default=func.now(),
        server_default=func.now(),
    )
    updated_at = Column(
        DateTime,
        default=func.now(),
        server_default=func.now(),
        server_onupdate=func.now(),
    )
