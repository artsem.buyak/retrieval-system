import enum

from sqlalchemy import Column, Enum, ForeignKey, Integer, String, Text
from sqlalchemy.orm import relationship

from db.models.base import Base, CreateUpdateMixin


class StatusEnum(str, enum.Enum):
    PROCESSING = "processing"
    FAILED = "failed"
    FINISHED = "finished"


class Page(Base, CreateUpdateMixin):
    """
    Page Model for storing processed html pages
    """

    __tablename__ = "pages"  # type: ignore

    id = Column(
        Integer,
        primary_key=True,
        autoincrement=True,
        index=True,
    )
    url = Column(
        Text,
        nullable=False,
        unique=True,
        index=True,
    )
    checksum = Column(
        String(255),
        unique=True,
    )

    def __repr__(self):
        return self.url


class PageTask(Base, CreateUpdateMixin):
    """
    PageTasks Model for storing history of processing html page
    """

    __tablename__ = "page_tasks"  # type: ignore

    id = Column(
        Integer,
        primary_key=True,
        autoincrement=True,
        index=True,
    )

    status = Column(
        Enum(
            StatusEnum,
            values_callable=lambda obj: [e.value for e in obj],
        ),
        default=StatusEnum.PROCESSING,
        index=True,
    )

    error_details = Column(
        Text,
        nullable=True,
    )

    page_id = Column(
        Integer,
        ForeignKey(
            Page.id,
            ondelete="CASCADE",
        ),
        index=True,
        nullable=True,
    )
    page = relationship(
        Page,
        backref="tasks",
    )

    def __repr__(self):
        return f"{self.page_id}: {self.status}"


class Paragraph(Base, CreateUpdateMixin):
    """
    PageParagraph Model, which store paragraph info from Page
    """

    __tablename__ = "paragraphs"  # type: ignore

    id = Column(
        Integer,
        primary_key=True,
        autoincrement=True,
        index=True,
    )
    text = Column(
        Text,
        index=True,
    )

    order = Column(Integer)

    page_id = Column(
        Integer,
        ForeignKey(
            Page.id,
            ondelete="CASCADE",
        ),
        index=True,
        nullable=True,
    )
    page = relationship(
        Page,
        backref="paragraphs",
    )

    def __repr__(self):
        return self.text


class ParagraphFact(Base, CreateUpdateMixin):
    """
    ParagraphFact Model, which store extracted paragraph facts
    """

    __tablename__ = "paragraph_facts"  # type: ignore

    id = Column(
        Integer,
        primary_key=True,
        autoincrement=True,
        index=True,
    )
    text = Column(
        Text,
        index=True,
    )
    embedding = Column(Text, default=[])

    paragraph_id = Column(
        Integer,
        ForeignKey(
            Paragraph.id,
            ondelete="CASCADE",
        ),
        index=True,
        nullable=True,
    )
    paragraph = relationship(
        Paragraph,
        backref="facts",
    )

    def __repr__(self):
        return self.text
