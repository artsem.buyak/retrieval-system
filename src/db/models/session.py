from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.orm import sessionmaker

from core.config import settings

SQLALCHEMY_DATABASE_URL = settings.SQLALCHEMY_DATABASE_URI


engine = create_async_engine(SQLALCHEMY_DATABASE_URL, echo=True)
DbSession = sessionmaker(engine, class_=AsyncSession, expire_on_commit=False)
