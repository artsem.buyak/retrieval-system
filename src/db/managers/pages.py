from typing import Iterable

from sqlalchemy import delete
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.future import select
from sqlalchemy.orm import joinedload

from db.managers.base import BaseManager
from db.models import Page, PageTask, Paragraph, ParagraphFact
from schemas import (
    PageCreate,
    PageTaskCreate,
    PageTaskUpdate,
    PageUpdate,
    ParagraphCreate,
    ParagraphFactCreate,
)


class PageManager(BaseManager[Page, PageCreate, PageUpdate]):
    async def get_by_url(
        self,
        session: AsyncSession,
        *,
        url: str,
    ) -> Page | None:
        result = await session.execute(select(Page).where(Page.url == url))
        return result.scalar()


class PageTaskManager(BaseManager[PageTask, PageTaskCreate, PageTaskUpdate]):
    async def get_multi_by_page_id(
        self,
        session: AsyncSession,
        *,
        page_id: int,
    ) -> Page | None:
        result = await session.execute(
            select(PageTask).where(PageTask.page_id == page_id).order_by(PageTask.created_at)
        )
        return result.scalars().all()


class ParagraphManager(BaseManager[Paragraph, ParagraphCreate, None]):
    async def remove_by_page_id(
        self,
        session: AsyncSession,
        *,
        page_id: int,
    ) -> None:
        await session.execute(delete(self.model).where(self.model.page_id == page_id))
        await session.commit()

    async def get_multi_by_page_id(
        self,
        session: AsyncSession,
        *,
        page_id: int,
    ) -> list[Paragraph]:
        result = await session.execute(
            select(self.model).where(self.model.page_id == page_id).order_by(self.model.created_at)
        )
        return result.scalars().all()

    async def update(self, *args, **kwargs) -> None:  # type: ignore
        raise NotImplementedError


class ParagraphFactManager(BaseManager[ParagraphFact, ParagraphFactCreate, None]):
    async def remove_by_paragraphs_ids(
        self,
        session: AsyncSession,
        *,
        paragraphs_ids: list[int],
    ) -> None:
        await session.execute(delete(self.model).where(self.model.paragraph_id.in_(paragraphs_ids)))
        await session.commit()

    async def get_multi_by_paragraphs_ids(
        self,
        session: AsyncSession,
        *,
        paragraphs_ids: list[int],
        skip: int = 0,
        limit: int = 100,
    ) -> list[ParagraphFact]:
        result = await session.execute(
            select(self.model).where(self.model.paragraph_id.in_(paragraphs_ids)).offset(skip).limit(limit)
        )
        return result.scalars().all()

    async def get(
        self,
        session: AsyncSession,
        obj_id: int,
        use_paragraph_join=False,
    ) -> ParagraphFact | None:
        query = select(self.model).where(self.model.id == obj_id)

        if use_paragraph_join:
            query = query.options(
                joinedload(self.model.paragraph),
            )

        result = await session.execute(query)
        return result.scalar()

    async def get_multi_by_paragraph_id(
        self,
        session: AsyncSession,
        *,
        paragraph_id: int,
    ) -> Page | None:
        result = await session.execute(
            select(self.model).where(self.model.paragraph_id == paragraph_id).order_by(self.model.created_at)
        )
        return result.scalars().all()

    async def batch_create(
        self,
        session: AsyncSession,
        paragraph_id: int,
        facts_embeddings: Iterable[tuple[str, list[float]]],
    ):
        objs = [
            ParagraphFact(
                text=fact,
                embedding=str(embedding),
                paragraph_id=paragraph_id,
            )
            for fact, embedding in facts_embeddings
        ]

        session.add_all(objs)
        await session.commit()

        for db_obj in objs:
            await session.refresh(db_obj)

        return objs

    async def update(self, *args, **kwargs) -> None:  # type: ignore
        raise NotImplementedError


page_manager = PageManager(Page)
page_task_manager = PageTaskManager(PageTask)
paragraph_manager = ParagraphManager(Paragraph)
paragraph_fact_manager = ParagraphFactManager(ParagraphFact)
