import os
import secrets
from typing import Any

from pydantic import AnyHttpUrl, BaseSettings, PostgresDsn, validator


class Settings(BaseSettings):
    API_STR: str = "/api/"
    BACKEND_CORS_ORIGINS: list[AnyHttpUrl] = []
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

    SECRET_KEY: str = secrets.token_urlsafe(32)

    AVAILABLE_DOMAINS: list[str] = []

    PROJECT_NAME: str = "src"

    # DB connection config
    POSTGRES_SERVER: str = "localhost"
    POSTGRES_USER: str = "postgres"
    POSTGRES_PASSWORD: str = "postgres"
    POSTGRES_DB: str = "retrieval_system_db"
    POSTGRES_PORT: int = 5432
    SQLALCHEMY_DATABASE_URI: PostgresDsn | None = None

    # s3 connection config
    AWS_REQUEST_PAYER: str = "requester"
    AWS_ACCESS_KEY_ID: str = ""
    AWS_SECRET_ACCESS_KEY: str = ""
    AWS_S3_ENDPOINT_URL: str | None = None
    AWS_S3_BUCKET_NAME: str = "retrieval-system-data"

    # Pinecone connection config
    PINECONE_ENV: str = "us-west4-gcp-free"
    PINECONE_API_KEY: str = "1186402f-6b8b-438e-aaba-b91c3f47103f"
    PINECONE_INDEX_NAME: str = "retrieval-system-index"
    PINECONE_INDEX_DIMENSION: int = 768
    PINECONE_PROJECT_ID: str = "5cee1ee"
    PINECONE_DEFAULT_NAMESPACE: str = "default-namespace"
    SERVER_HOST: str = "localhost:8000"

    OPENAI_KEY: str = ""
    SCORE_THRESHOLD = 0.50

    @validator("BACKEND_CORS_ORIGINS", pre=True)
    def assemble_cors_origins(cls, v: str | list[str]) -> list[str] | str:
        if isinstance(v, str) and not v.startswith("["):
            return [i.strip() for i in v.split(",")]
        elif isinstance(v, (list, str)):
            return v
        raise ValueError(v)

    @validator("SQLALCHEMY_DATABASE_URI", pre=True)
    def assemble_db_connection(cls, v: str | None, values: dict[str, Any]) -> Any:
        if isinstance(v, str):
            return v
        return PostgresDsn.build(
            scheme="postgresql+asyncpg",
            user=values.get("POSTGRES_USER"),
            password=values.get("POSTGRES_PASSWORD"),
            host=values.get("POSTGRES_SERVER"),
            path=f":{values.get('POSTGRES_PORT')}/{values.get('POSTGRES_DB') or ''}",
        )

    class Config:
        case_sensitive = True
        env_file = "env/.env"


settings = Settings()
