import logging
import os

import gradio as gr
import requests
import uvicorn
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from api.v1 import api_router as api_router_v1
from clients.pinecone import pinecone_client
from core.config import settings


def get_application():
    _app = FastAPI(
        title=settings.PROJECT_NAME,
        openapi_url=f"{settings.API_STR}/openapi.json",
    )

    _app.add_middleware(
        CORSMiddleware,
        allow_origins=[str(origin) for origin in settings.BACKEND_CORS_ORIGINS],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

    return _app


app = get_application()
app.include_router(api_router_v1, prefix=os.path.join(settings.API_STR, "v1"))


def gradio_get_answer(question: str) -> str:
    url = app.url_path_for("fact-retrieval-create")

    response = requests.post(
        f"http://{settings.SERVER_HOST}{url}",
        json={
            "question": str(question),
        },
    )

    return response.text


io = gr.Interface(
    fn=gradio_get_answer,
    inputs=gr.Textbox(label="Ask a question", placeholder="Tell me something about bear?"),
    outputs=[gr.Textbox(label="Answer")],
    allow_flagging="never",
)
gradio_app = gr.routes.App.create_app(io)

app.mount("/gradio", gradio_app)

# setup loggers
logging.config.fileConfig(
    os.path.join(os.path.dirname(settings.BASE_DIR), "log.ini"),
    disable_existing_loggers=False,
)


@app.on_event("startup")
async def startup():
    """
    Fetch index info or create new one if not exist
    """

    index = await pinecone_client.describe_index()

    if not index:
        pinecone_client.create_index(
            index_name=settings.PINECONE_INDEX_NAME,
        )


@app.on_event("shutdown")
async def shutdown():
    print("shutdown")


if __name__ == "__main__":
    uvicorn.run("main:app", host="0.0.0.0", port=8000)  # nosec
