from http import HTTPStatus
from unittest.mock import patch

import pytest

from api.v1.tasks.page_info_processing import parse_info_by_url
from tests.base import AsyncMock

WIKI_TEST_URL = "https://en.wikipedia.org/wiki/Bear"


with open("tests/mocked_data/test.html", "r") as file:
    test_page_data = file.read()


class TestWebScrapper:
    @patch("fastapi.BackgroundTasks.add_task")
    async def test_scrapper_request__ok__(self, background_task_mock, app, async_client, session):
        """
        Test web_scrapper
        """

        url = app.url_path_for("url-parse-create")

        await async_client.post(
            url,
            json={
                "source_url": WIKI_TEST_URL,
            },
        )

    @pytest.mark.parametrize(
        "input_params,expected",
        [
            (
                {
                    "source_url": None,
                },
                {"status_code": HTTPStatus.UNPROCESSABLE_ENTITY, "msg": "none is not an allowed value"},
            ),
            ({}, {"status_code": HTTPStatus.UNPROCESSABLE_ENTITY, "msg": "field required"}),
        ],
    )
    async def test_scrapper_request__fail__unprocessable_entity(
        self,
        app,
        async_client,
        session,
        input_params,
        expected,
    ):
        """
        Test web_scrapper with unprocessable entity values
        """

        url = app.url_path_for("url-parse-create")

        response = await async_client.post(
            url,
            json=input_params,
        )

        assert response.status_code == expected["status_code"]
        response_data = response.json()
        assert response_data["detail"][0]["msg"] == expected["msg"]

    async def test_scrapper_request__fail__bad_request_not_wiki(
        self,
        app,
        async_client,
        session,
    ):
        """
        Test web_scrapper with not wiki domain
        """

        url = app.url_path_for("url-parse-create")

        response = await async_client.post(
            url,
            json={
                "source_url": "https://tesst.com/Bear",
            },
        )

        assert response.status_code == HTTPStatus.BAD_REQUEST
        response_data = response.json()
        assert response_data["detail"] == "Sorry, currently we support urls only from wikipedia domain."

    @patch(
        "api.v1.tasks.page_info_processing.is_page_processed_before",
        new_callable=AsyncMock,
        return_value=[..., ..., True],
    )
    @patch("api.v1.tasks.page_info_processing.download_page_by_url", return_value=test_page_data)
    async def test_parse_info_by_url__ok__processed_before(
        self,
        request_mock,
        processed_check_mock,
        session,
    ):
        """
        parse_info_by_url run test
        """

        await parse_info_by_url(WIKI_TEST_URL, session)

    @patch(
        "api.v1.tasks.page_info_processing.is_page_processed_before",
        new_callable=AsyncMock,
        return_value=[..., ..., False],
    )
    @patch("api.v1.tasks.page_info_processing.download_page_by_url", return_value=test_page_data)
    @patch(
        "db.managers.pages.paragraph_manager.create",
        new_callable=AsyncMock,
    )
    @patch(
        "api.v1.tasks.page_info_processing.processing_paragraph_info",
        new_callable=AsyncMock,
    )
    @patch(
        "db.managers.pages.page_task_manager.update",
        new_callable=AsyncMock,
    )
    async def test_parse_info_by_url__ok__(
        self,
        page_task_manager_update_mock,
        processing_paragraph_info_mock,
        paragraph_manager_create_mock,
        request_mock,
        processed_check_mock,
        session,
    ):
        """
        parse_info_by_url run test
        """

        await parse_info_by_url(WIKI_TEST_URL, session)
