from http import HTTPStatus
from unittest.mock import patch

import pytest

from api.v1.endpoints.facts_retrieval import NOT_FOUND_RESPONSES
from tests.base import AsyncMock


class ParagraphMock:
    def __init__(self, text: str):
        self.text = text


class FactMock:
    def __init__(self, text: str, paragraph_text: str):
        self.text = text
        self.paragraph = ParagraphMock(paragraph_text)


class MatchMock:
    def __init__(self, mock_id: int, score: float):
        self.id = mock_id
        self.score = score


class PineconeMock:
    def __init__(self, match_id: int, score: float):
        self.matches = [MatchMock(mock_id=match_id, score=score)]


def response_generator_mock():
    for item in ["test", " mocked", " value"]:
        yield item


class TestRetrievalLogic:
    @pytest.mark.parametrize(
        "input_params,expected",
        [
            (
                {
                    "question": None,
                },
                {"status_code": HTTPStatus.UNPROCESSABLE_ENTITY, "msg": "field required"},
            ),
            ({}, {"status_code": HTTPStatus.UNPROCESSABLE_ENTITY, "msg": "field required"}),
        ],
    )
    async def test_retrieval_endpoint__fail__unprocessable_entity(
        self,
        app,
        async_client,
        session,
        input_params,
        expected,
    ):
        """
        Test retrieval endpoint with unprocessable entity values
        """

        url = app.url_path_for("fact-retrieval-create")

        response = await async_client.post(
            url,
            json=input_params,
        )

        assert response.status_code == expected["status_code"]
        response_data = response.json()
        assert response_data["detail"][0]["msg"] == expected["msg"]

    @patch(
        "ml_tools.response_generation.response_generator.generate",
        return_value=response_generator_mock(),
    )
    @patch(
        "clients.pinecone.pinecone_client.search_by_vector",
        new_callable=AsyncMock,
        return_value=PineconeMock(match_id=1, score=0.75),
    )
    @patch(
        "ml_tools.onnx_text_embedding.text_embedder.generate",
        new_callable=AsyncMock,
    )
    @patch(
        "db.managers.pages.paragraph_fact_manager.get",
        new_callable=AsyncMock,
        return_value=FactMock(text="fact text", paragraph_text="paragraph text"),
    )
    async def test_retrieval_endpoint__ok__(
        self,
        paragraph_fact_manager_mock,
        text_embedder_generate_mock,
        pinecone_client_search_by_vector_mock,
        response_generator_generate_mock,
        app,
        async_client,
        session,
    ):
        """
        Test retrieval endpoint
        """

        url = app.url_path_for("fact-retrieval-create")

        response = await async_client.post(
            url,
            json={
                "question": "Tell me something",
            },
        )

        assert response.status_code == HTTPStatus.OK
        response_data = response.text

        assert response_data == "test mocked value"

    @patch(
        "ml_tools.response_generation.response_generator.generate",
        return_value=response_generator_mock(),
    )
    @patch(
        "clients.pinecone.pinecone_client.search_by_vector",
        new_callable=AsyncMock,
        return_value=PineconeMock(match_id=1, score=0.49),
    )
    @patch(
        "ml_tools.onnx_text_embedding.text_embedder.generate",
        new_callable=AsyncMock,
    )
    @patch(
        "db.managers.pages.paragraph_fact_manager.get",
        new_callable=AsyncMock,
        return_value=FactMock(text="fact text", paragraph_text="paragraph text"),
    )
    async def test_retrieval_endpoint__ok__less_then_threshold(
        self,
        paragraph_fact_manager_mock,
        text_embedder_generate_mock,
        pinecone_client_search_by_vector_mock,
        response_generator_generate_mock,
        app,
        async_client,
        session,
    ):
        """
        Test retrieval endpoint
        """

        url = app.url_path_for("fact-retrieval-create")

        response = await async_client.post(
            url,
            json={
                "question": "Tell me something",
            },
        )

        assert response.status_code == HTTPStatus.OK
        assert response.text.strip() in NOT_FOUND_RESPONSES
